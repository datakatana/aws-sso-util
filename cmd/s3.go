package cmd

import (
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/spf13/cobra"
)

// s3Cmd represents the s3 command
var s3Cmd = &cobra.Command{
	Use:   "s3",
	Short: "Interact with AWS S3",
	Long:  `This command allows you to interact with AWS S3. For now, it lists all the S3 buckets.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Initialize a session
		sess, err := session.NewSession(&aws.Config{
			Region: aws.String("us-west-1"), // Change this to your preferred region
		})

		if err != nil {
			log.Fatalf("Failed to create session: %s", err)
		}

		// Create an S3 service client
		s3Svc := s3.New(sess)

		// List buckets
		result, err := s3Svc.ListBuckets(nil)
		if err != nil {
			log.Fatalf("Failed to list buckets: %s", err)
		}

		fmt.Println("Buckets:")
		for _, b := range result.Buckets {
			fmt.Printf("* %s\n", aws.StringValue(b.Name))
		}
	},
}

func init() {
	rootCmd.AddCommand(s3Cmd)
}
