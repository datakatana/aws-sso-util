/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "aws-sso-util/cmd"

func main() {
	cmd.Execute()
}
